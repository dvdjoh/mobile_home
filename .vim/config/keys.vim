" Leader stuff
nnoremap <Space> <Nop>
let mapleader=" "

" Open the vimrc
nnoremap <Leader>vim :e $MYVIMRC<Cr>

" Highlight the whole buffer
nnoremap <Leader>a ggVG
" Open a new tab
nnoremap <Leader>te :tab split<Cr>
nnoremap <Leader>gt :tab split<Cr>
" Close the current tab
nnoremap <Leader>tc :tabc<Cr>
nnoremap <Leader>tk :tabc<Cr>

" Open a terminal in the current split
nnoremap <Leader><Cr> :ter ++curwin<Cr>

" Open scratch buffer in split below
nnoremap gs :sp<Cr><C-w><C-j>:Scratch<Cr>

" Escape with kj, jk, JK, and KJ
let g:easyescape_chars = { "j": 1, "k": 1}
let g:easyescape_timeout = 50
cnoremap jk <ESC>
cnoremap kj <ESC>

" Terminal mappings
tnoremap <C-n> <C-\><C-n>

" Split controls
nnoremap <Leader>h :vsp<Cr>
nnoremap <Leader>j :sp<Cr><C-w><C-j>
nnoremap <Leader>k :sp<Cr>
nnoremap <Leader>l :vsp<Cr><C-w><C-l>

" Faster split movement
nnoremap  <C-h> <C-w>h
nnoremap  <C-j> <C-w>j
nnoremap  <C-k> <C-w>k
nnoremap  <C-l> <C-w>l
inoremap  <C-h> <Esc><C-w>h
inoremap  <C-j> <Esc><C-w>j
inoremap  <C-k> <Esc><C-w>k
inoremap  <C-l> <Esc><C-w>l

" toggle semantic highlighting
nnoremap <Leader>s :SemanticHighlightToggle<Cr>

" Bring Y in line with D and C
nnoremap Y y$

" Fuzzy finding
" Files
nnoremap <Leader>e :Files<Cr>
" Buffers
nnoremap <Leader>b :Buffers<Cr>
" Tags
nnoremap <Leader>/ :Ag<Cr>
" Ag cwd
nnoremap <leader>f :Tags<Cr>

" Toggle between header and source
autocmd Filetype cpp,c nnoremap <Leader>o :call ToggleHCPP()<Cr>

" Cut, copy, and paste from the clipboard more easily
nnoremap <Leader>d "+d
nnoremap <Leader>D "+D
nnoremap <Leader>y "+y
nnoremap <Leader>Y "+Y
nnoremap <Leader>p "+p

" Map OverCommandLine for substitute previewing
nnoremap s :OverCommandLine<Cr>%s/
vnoremap s :OverCommandLine<Cr>s/

" For doing stuff with what's under the cursor
nnoremap <leader>8 :Ag <C-r><C-w><Cr>

" CD COMMANDS
nnoremap cdh  :cd %:p:h<Cr>
nnoremap cd.. :cd ..<Cr>
nnoremap cd-  :cd -<Cr>
nnoremap cd~  :cd %:p:h/..<Cr>
nnoremap cd<Space> :cd 

" swap gj j, gk k
nnoremap j gj
nnoremap gj j
nnoremap k gk
nnoremap gk k

" Snippet mapping
let g:UltiSnipsExpandTrigger="<C-k>"
let g:UltiSnipsJumpForwardTrigger="<C-n>"
let g:UltiSnipsJumpBackwardTrigger="<C-p>"

" Shorten window swapping command
let g:windowswap_map_keys = 0 " prevent default bindings
nnoremap <silent> <leader>w :call WindowSwap#EasyWindowSwap()<CR>

" For conceal markers.
if has('conceal')
  set conceallevel=2 concealcursor=niv
endif
