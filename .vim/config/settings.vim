if has('win32')
    colorscheme desert
endif

" Cool colors
set termguicolors
colorscheme gruvbox
set background=dark

" Search all lower = insensitive, any upper = sensitive
set ignorecase
set smartcase
set hlsearch
" Use filetype specific indentation
set expandtab
set shiftwidth=4
set tabstop=4
filetype plugin indent on
" hide buffers that are unsaved
set hidden
" disable annoying sticky comments
autocmd BufNewFile,BufRead * setlocal formatoptions-=ro
" wildmode more like bash (tab keeps completing as long as it's unambiguous)
set wildmode=list,longest
" make the mouse work in console vim
set mouse=a

" Gui options
if has("gui_running")
    " Use non-gui tabline
    set guioptions-=e
    " gvim settings to remove menu bar and toolbar
    set guioptions-=m
    set guioptions-=T
    " gvim remove lefthand scrollbar
    set guioptions-=L
    set guioptions-=r
    " set gvim font
    set guifont=Source\ Code\ Pro\ 13,Consolas:h14:cANSI
endif

" associate .p8 files with the lua filetype
au BufNewFile,BufRead *.p8 setlocal ft=lua

" Line highlights
" Finished
au BufRead,BufNewFile *.txt   syntax match FinishedMatch "+ .*"
hi def  FinishedColor   ctermbg=darkgreen ctermfg=white    guibg=darkgreen guifg=lightgreen
hi link FinishedMatch FinishedColor

" NotDoing
au BufRead,BufNewFile *.txt   syntax match NotDoingMatch "\~ .*"
hi def  NotDoingColor   ctermbg=darkred ctermfg=white    guibg=darkred guifg=lightred
hi link NotDoingMatch NotDoingColor

" Started
au BufRead,BufNewFile *.txt   syntax match StrikeoutMatch "\. .*"
hi def  StrikeoutColor   ctermbg=yellow ctermfg=white    guibg=#cc6600 guifg=pink
hi link StrikeoutMatch StrikeoutColor

" Title
au BufRead,BufNewFile *.txt   syntax match TitleMatch "_ .*"
hi def  TitleColor   ctermbg=white ctermfg=black    guibg=white guifg=black
hi link TitleMatch TitleColor

" Auto format elm code
let g:elm_format_autosave = 1

" Copied from fzf.vim docs
" Enable per-command history.
" CTRL-N and CTRL-P will be automatically bound to next-history and
" previous-history instead of down and up. If you don't like the change,
" explicitly bind the keys to down and up in your $FZF_DEFAULT_OPTS.
let g:fzf_history_dir = '~/.local/share/fzf-history'

" Make fzf Ag ignore filenames
command! -bang -nargs=* Ag call fzf#vim#ag(<q-args>, {'options': '--delimiter : --nth 4..'}, <bang>0)

" CTRL-A CTRL-Q to select all and build quickfix list
function! s:build_quickfix_list(lines)
    call setqflist(map(copy(a:lines), '{ "filename": v:val }'))
    copen
    cc
endfunction

let g:fzf_action = {
            \ 'ctrl-q': function ('s:build_quickfix_list'),
            \ 'ctrl-t': 'tab split',
            \ 'ctrl-x': 'split',
            \ 'ctrl-v': 'vsplit' }

let $FZF_DEFAULT_OPTS = '--bind ctrl-a:select-all'
