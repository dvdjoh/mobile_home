" Plugins
call plug#begin()
" Color scheme stuff
Plug 'sheerun/vim-polyglot'
Plug 'trevordmiller/nova-vim'
Plug 'morhetz/gruvbox'
Plug 'romainl/Apprentice'

" Semantic highlighting
Plug 'jaxbot/semantic-highlight.vim'

""""" TPOPE stuff
" Async dispatch from vim 7.x
Plug 'tpope/vim-dispatch'
" Make netrw a little nicer
Plug 'tpope/vim-vinegar'
" More functionality for changing text surrounded by stuff
Plug 'tpope/vim-surround'
" Doing git stuff from vim
Plug 'tpope/vim-fugitive'
" Make <C-a> and <C-x> work better with dates
Plug 'tpope/vim-speeddating'
" Make . work for some plugin action too (by default surround, speeddating, unimpaired, easyclip)
Plug 'tpope/vim-repeat'
" Expand the mappings for [] movements
Plug 'tpope/vim-unimpaired'
" Sensible defaults, replacing a lot of my settings
Plug 'tpope/vim-sensible'
" Comment with gc
Plug 'tpope/vim-commentary'

" fzf
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

" Interactivity with search and replace
Plug 'osyo-manga/vim-over'

" Snippets
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

" Diff swap files
Plug 'chrisbra/Recover.vim'

" Swap splits without thinking about layout
Plug 'wesQ3/vim-windowswap'

" Make jk escaping better
Plug 'zhou13/vim-easyescape'

" Better hlsearch behavior
Plug 'romainl/vim-cool'

" Plug 'elmcast/elm-vim'
call plug#end()
