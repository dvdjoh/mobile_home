" Toggle header and soruce
function! ToggleHCPP()
    if expand('%:e') == "h"
        :e %<.cpp
    else
        :e %<.h
    endif
endfunction

" Open or create scratch buffer
function! ScratchOpen()
    let sc = "scratch"
    if bufwinnr(sc) > 0
        :b scratch
    else
        :e scratch
    endif
    :set readonly
    :set nobl
endfunction
command! Scratch :call ScratchOpen()

" Open the current buffer in a new tab and call git status
function! GS()
    :tab split
    :Gstatus
endfunction
command! GS :call GS()
